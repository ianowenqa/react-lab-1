import React, { Component } from 'react';
import UserDetail from './UserDetail';

class User extends Component {

    render() {
        // This is dummy data!
        const myUserData = {
            firstName: 'John',
            lastName: 'Doe',
            address: {
                city: 'Salford'
            },
            dob: new Date('1990-01-01')
        };

        return (
            <div>
                <h1>User</h1>
                <UserDetail userData={myUserData} />
            </div>
        );
    }
}

export default User;
