import React, { Component } from 'react';

class UserDetail extends Component {
    render() {
        // If you're getting lots of nested data it's a good idea to use destructuring.
        // It comes with the added bonus of letting you default properties if they don't exist!
        const {
            firstName = 'N/A',
            lastName = 'N/A',
            dob,
            address: { city }
        } = this.props.userData;

        // Format the Date object since toString() has too much detail.
        // Moment.js would make this much simpler
        let formattedDate = 'N/A';
        if (dob) {
            const day = dob.getDate().toString().padStart(2, '0'),
                month = (dob.getMonth() + 1).toString().padStart(2, '0'), // Months start at 0 for no good reason
                year = dob.getFullYear()
            formattedDate = `${day}/${month}/${year}`;
        }

        return (
            <ul>
                <li>First Name: {firstName}</li>
                <li>Last Name: {lastName}</li>
                <li>Date of birth: {formattedDate}</li>
                <li>City: {city}</li>
            </ul>
        );
    }
}

export default UserDetail;


